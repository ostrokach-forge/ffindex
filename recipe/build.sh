#!/bin/sh

set -ev

# export CXXFLAGS=$(echo "${CXXFLAGS}" | sed 's/std=c++17/std=c++14/g')
# export DEBUG_CXXFLAGS=$(echo "${DEBUG_CXXFLAGS}" | sed 's/std=c++17/std=c++14/g')

# Required if we are downloading a zip archive of the repo
pwd
ls

mkdir build
cd build
cmake \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -G "Unix Makefiles" \
  -DCMAKE_INSTALL_PREFIX=${PREFIX} \
  ..
make
make install
